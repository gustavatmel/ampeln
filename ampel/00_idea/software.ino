

#define LIGHT_GREEN 6
#define LIGHT_ORANGE 7
#define LIGHT_RED 8
#define BUTTON_GREEN 9
#define BUTTON_ORANGE 10
#define BUTTON_RED 11

uint32_t dur_green = 0;
uint32_t dur_orange = 0;
uint32_t dur_red = 0;

  
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin as an output.
  pinMode(LIGHT_GREEN, OUTPUT);
  pinMode(LIGHT_ORANGE, OUTPUT);
  pinMode(LIGHT_RED, OUTPUT);
  pinMode(BUTTON_GREEN, INPUT_PULLUP);
  pinMode(BUTTON_ORANGE, INPUT_PULLUP);
  pinMode(BUTTON_RED, INPUT_PULLUP);

}

// the loop function runs over and over again forever
void loop() {
 if(digitalRead(BUTTON_GREEN) == LOW) {
  dur_green++;
  digitalWrite(LIGHT_GREEN, HIGH);
 }
 else {
  dur_green = 0;
  digitalWrite(LIGHT_GREEN, LOW);
 }

 if(digitalRead(BUTTON_ORANGE) == LOW) {
  dur_orange++;
  digitalWrite(LIGHT_ORANGE, HIGH);
 }
 else {
  dur_orange = 0;
  digitalWrite(LIGHT_ORANGE, LOW);
 }
 
 if(digitalRead(BUTTON_RED) == LOW) {
  dur_red++;
  digitalWrite(LIGHT_RED, HIGH);
 }
 else {
  dur_red = 0;
  digitalWrite(LIGHT_RED, LOW);
 }

 if (dur_green >= 20 && dur_orange >= 20 && dur_red >= 20) {
  traffic_mode();
 }

 if (dur_green >= 20 && dur_orange >= 20 && dur_red >= 0) {
  disco_mode();
 }

 if (dur_green >= 0 && dur_orange >= 20 && dur_red >= 20) {
  midnight_mode();
 }

 delay(100);
}



void traffic_mode() {
  change_mode();
  while (digitalRead(BUTTON_GREEN) && digitalRead(BUTTON_ORANGE) && digitalRead(BUTTON_RED)) {
    digitalWrite(LIGHT_RED, HIGH);
    delay(random(5000, 10000));
    digitalWrite(LIGHT_ORANGE, HIGH);
    delay(1500);
    digitalWrite(LIGHT_RED, LOW);
    digitalWrite(LIGHT_ORANGE, LOW);
    digitalWrite(LIGHT_GREEN, HIGH);
    delay(random(5000, 15000));
    digitalWrite(LIGHT_GREEN, LOW);
    digitalWrite(LIGHT_ORANGE, HIGH);
    delay(1500);
    digitalWrite(LIGHT_ORANGE, LOW);
  } 
  change_mode();
  return;
}

void disco_mode() {
  change_mode();
  while (digitalRead(BUTTON_GREEN) && digitalRead(BUTTON_ORANGE) && digitalRead(BUTTON_RED)) {
    digitalWrite(random(LIGHT_GREEN, LIGHT_RED+1), HIGH);
    digitalWrite(random(LIGHT_GREEN, LIGHT_RED+1), HIGH);
    delay(random(200, 800));
    clear_all();
  } 
  change_mode();
  return;
}

void midnight_mode() {
  change_mode();
  while (digitalRead(BUTTON_GREEN) && digitalRead(BUTTON_ORANGE) && digitalRead(BUTTON_RED)) {
    digitalWrite(LIGHT_ORANGE, !digitalRead(LIGHT_ORANGE));
    delay(500);
  } 
  change_mode();
  return;
}

void change_mode() {
  dur_green = 0;
  dur_orange = 0;
  dur_red = 0;
  digitalWrite(LIGHT_RED, HIGH);
  digitalWrite(LIGHT_ORANGE, HIGH);
  digitalWrite(LIGHT_GREEN, HIGH);
  delay(500);
  digitalWrite(LIGHT_RED, LOW);
  digitalWrite(LIGHT_ORANGE, LOW);
  digitalWrite(LIGHT_GREEN, LOW);
  delay(500);
  digitalWrite(LIGHT_RED, HIGH);
  digitalWrite(LIGHT_ORANGE, HIGH);
  digitalWrite(LIGHT_GREEN, HIGH);
  delay(500);
  digitalWrite(LIGHT_RED, LOW);
  digitalWrite(LIGHT_ORANGE, LOW);
  digitalWrite(LIGHT_GREEN, LOW);
  delay(500);
}

void clear_all() {
  digitalWrite(LIGHT_RED, LOW);
  digitalWrite(LIGHT_ORANGE, LOW);
  digitalWrite(LIGHT_GREEN, LOW);
}
