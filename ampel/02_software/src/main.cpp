#include "Arduino.h"
#include <JC_Button.h>          // https://github.com/JChristensen/JC_Button

#define LIGHT_GREEN 6
#define LIGHT_ORANGE 7
#define LIGHT_RED 8
#define BUTTON_GREEN 9
#define BUTTON_ORANGE 10
#define BUTTON_RED 11

Button btnG(BUTTON_GREEN);       // define the button
Button btnO(BUTTON_ORANGE);       // define the button
Button btnR(BUTTON_RED);       // define the button


const unsigned long LONG_PRESS(2000);          // we define a "long press" to be 1000 milliseconds.
  
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin as an output.
  pinMode(LIGHT_GREEN, OUTPUT);
  pinMode(LIGHT_ORANGE, OUTPUT);
  pinMode(LIGHT_RED, OUTPUT);

  btnG.begin();
  btnO.begin();
  btnR.begin();

  Serial.begin(115200);

}

enum states_t {LIGHT_KEEP, LIGHT_PRESS};

bool ampelState;    

void loop()
{
	static states_t STATE;      // current state machine state
	
	btnG.read();                   // read the buttons
    btnO.read();
    btnR.read();

	Serial.println(btnG.pressedFor(2000)); 

	switch (STATE)
    {

        case LIGHT_KEEP:
        	Serial.println("entered LIGHT_KEEP"); 
            if (btnG.wasPressed())
                digitalWrite(LIGHT_GREEN, !digitalRead(LIGHT_GREEN));
            else if (btnO.wasPressed())
                digitalWrite(LIGHT_ORANGE, !digitalRead(LIGHT_ORANGE));
            else if (btnR.wasPressed())
                digitalWrite(LIGHT_RED, !digitalRead(LIGHT_RED));
           	else if (btnG.pressedFor(2000)) {
                STATE = LIGHT_PRESS;
                digitalWrite(LIGHT_GREEN, HIGH);
                digitalWrite(LIGHT_RED, HIGH);
                delay(1000);
                digitalWrite(LIGHT_GREEN, LOW);
                digitalWrite(LIGHT_RED, LOW);
                delay(1000);
            }
            break;

        case LIGHT_PRESS:
        	Serial.println("entered LIGHT_PRESS"); 
        	digitalWrite(LIGHT_GREEN, LOW);
            digitalWrite(LIGHT_ORANGE, LOW);
            digitalWrite(LIGHT_RED, LOW);

            if (btnG.isPressed())
                digitalWrite(LIGHT_GREEN, HIGH);
            if (btnO.isPressed())
                digitalWrite(LIGHT_ORANGE, HIGH);
            if (btnR.isPressed())
                digitalWrite(LIGHT_RED, HIGH);
            
            if (btnG.pressedFor(2000)) {
                STATE = LIGHT_KEEP;
                digitalWrite(LIGHT_GREEN, HIGH);
                digitalWrite(LIGHT_RED, HIGH);
                delay(1000);
                digitalWrite(LIGHT_GREEN, LOW);
                digitalWrite(LIGHT_RED, LOW);
                delay(1000);
            }
            break;
    }

}