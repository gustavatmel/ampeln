#include <Bounce2.h>


#define BUTTON_AUS 6
#define BUTTON_EIN 7
#define BUTTON_FLASH_EIN 8
#define BUTTON_FLASH_AUS 9
#define LIGHT_GREEN 10
#define LIGHT_RED 11
#define LIGHT_BLUE 12

Bounce button_ein = Bounce(); // Instantiate a Bounce object
Bounce button_aus = Bounce(); // Instantiate a Bounce object
Bounce button_flash_ein = Bounce(); // Instantiate a Bounce object
Bounce button_flash_aus = Bounce(); // Instantiate a Bounce object

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin as an output.

  button_ein.attach(BUTTON_EIN,INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
  button_ein.interval(100); // Use a debounce interval of 25 milliseconds
  button_aus.attach(BUTTON_AUS,INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
  button_aus.interval(100); // Use a debounce interval of 25 milliseconds
  button_flash_ein.attach(BUTTON_FLASH_EIN,INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
  button_flash_ein.interval(100); // Use a debounce interval of 25 milliseconds
  button_flash_aus.attach(BUTTON_FLASH_AUS,INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
  button_flash_aus.interval(100); // Use a debounce interval of 25 milliseconds
  pinMode(LIGHT_GREEN, OUTPUT);
  pinMode(LIGHT_RED, OUTPUT);
  pinMode(LIGHT_BLUE, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  button_ein.update();
  button_aus.update();
  button_flash_ein.update();
  button_flash_aus.update();

  
  if(button_ein.fell()) {
    digitalWrite(LIGHT_GREEN,  !digitalRead(LIGHT_GREEN));
  }
  if(button_aus.rose()) {
    digitalWrite(LIGHT_RED,  !digitalRead(LIGHT_RED));
  }
  if(button_flash_ein.rose()){
    digitalWrite(LIGHT_BLUE, HIGH);
  }
  if(button_flash_aus.rose()){
    digitalWrite(LIGHT_BLUE, LOW);
  }
}
